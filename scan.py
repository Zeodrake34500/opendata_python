###
#   OpenDATA
#   Script pour scanner un dossier 
#   et preparer la mise a disposition des fichiers
###

# Importer les bibliotheques Python
import os
import datetime
import sqlite3
import shutil
import glob

# Importer les fonctions metiers
from fonctions.fonctions_communes import *
from fonctions.fonctions_bdd import *

# Declarer les variables utiles
## Les dictionnaires
conf = {}
conf_chargee = {}
fichiers = {}
fichier_recent = {}
donnees_scan = {}
donnees_dossiers = {}
## Les listes
liste_fichiers = list()
## Les entiers
fichier_id = 0
scan_id = 0
i = 0
j = 0
## Les booleens
verif_on = False
copie_on = False

# Definir les variables de configuration
conf['xml'] = "./configuration.xml"
conf['xml_charge'] = _charger_xml(conf['xml'])

conf['sqlite'] = "C:\\Users\\damien\\OneDrive\\Programmation\\python\\opendata\\opendata.sqlite"

# Se connecter a la base SQLITE
connecter = sqlite3.connect(conf['sqlite'])
connexion = connecter.cursor()

# Charger la configuration
conf_chargee = _bdd_conf_charger_configuration(connexion,connecter)

# Recuperer le datetime de debut
maintenant = datetime.datetime.now()
datetime_depart = maintenant.strftime("%m/%d/%Y,%H:%M:%S")
donnees_date_depart = datetime_depart.split(',')

# Donnees du scan en cours
donnees_scan = {
    'date_debut' : donnees_date_depart[0],
    'heure_debut' : donnees_date_depart[1],
    'datetime_depart' :  datetime_depart,
}

# TODO : faire une gestion multi-dossiers
# Charger les chemins des dossiers
# donnees_dossiers = _charger_dossiers(connexion,connecter)

# Lancer un scan
fichiers, liste_fichiers = _scanner_dossier(conf_chargee['depart']['valeur'])
nbre_fichiers = len(fichiers.keys())
# Enregistrer les fichiers dans la base de donnees
for f in fichiers:
    # Initialiser les variables en debut de cycle
    action_on = False
    # Verifier si le fichier a deja ete enregistre    
    action_on = _bdd_verifier_fichier_deja_present(connexion,connecter,fichiers[f])
    # Traiter ...
    if not action_on :
        # Ajouter un fichier a la BDD
        fichier_id = _bdd_ajouter_fichier(connexion,connecter,fichiers[f])
        # Copier le fichier
        copie_on = _copier_fichier(connexion,connecter,fichiers[f]['fichier'],conf_chargee['depart']['valeur'],conf_chargee['arrivee']['valeur'],'deplacer')
        i = i+1
    if i == 1: 
        # Creer un scan
        scan_id = _bdd_ajouter_scan(connexion,connecter,datetime_depart)
    # Evaluer le nombre de fichiers
    j = j+1
# Executer les transactions        
connecter.commit()








# Se deconnecter de la base SQLITE
connecter.close()