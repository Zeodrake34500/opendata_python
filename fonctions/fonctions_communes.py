###
#   OpenDATA : fonctions utiles
###

import os
import glob
import shutil
import datetime

from lxml import etree

# Transformer DICT en LIST
def _transformer_dict_list(d):
    l = [ [k,v] for k, v in d.items() ]
    return l

# Charger donnees du XML
def _charger_xml(xml):
    d = {}
    t = etree.parse(cf)
    lx = [
        ('bdd_chemin','configuration/bdd_chemin'),
        ('bdd_type','configuration/bdd/type'),
        ('bdd_version','configuration/bdd/version'),
    ]
    return d

# Scanner le dossier de depart
def _scanner_dossier(d):
    # Definir les variables utiles
    df = {}
    tpl = ()
    lst = []
    # Recuperer le contenu du dossier (liste des fichiers)
    sc = os.listdir(d)
    # Parcourir la liste des fichiers trouves
    for f in sc:
        # Creer le chemin absolu vers le fichier cible
        cf = d + '\\' + f
        # Rechercher les donnees de temps
        z = os.path.getmtime(cf)
        dt = datetime.datetime.fromtimestamp(z)
        t = dt.strftime("%d-%m-%Y %H:%M:%S")
        # Separer la date et l'heure au niveau du STRFTIME
        y = t.split(' ')
        # Mettre a jour le dictionnaire des donnees des fichiers
        tpl = (f,d+"\\"+f,dt,y[0],y[1])
        lst.append(tpl)
        df.update({
            f : {
                'fichier' : f,
                'chemin_fichier' : d + "\\" + f,
                'datetime' : dt,
                'date_creation' : y[0],
                'heure_creation' : y[1]
            }
        })
    return df, lst    

# Chercher le fichier le plus recent
def _chercher_fichier_recent(ch):
    # Rechercher le fichier le plus recent dans le dossier cible
    fr = max(os.listdir(ch), key=lambda f: os.path.getctime("{}/{}".format(ch, f)))
    # Creer le chemin absolu du fichier le plus recent
    cf= ch + "\\" + fr
    # Recuperer sa date et son heure de creation
    z = os.path.getmtime(cf)
    t = datetime.datetime.fromtimestamp(z)
    t = t.strftime("%d-%m-%Y %H:%M:%S")
    d = t.split(' ')
    # Renvoyer un dictionnaire Python
    df = {
        'fichier' : fr,
        'chemin' : ch,
        'chemin_fichier' : cf, 
        'date_complete' : t,
        'date_creation' : d[0],
        'heure_creation' : d[1]
    }
    return df

# Copier le fichier du dossier source vers le dossier de destination
def _copier_fichier(cx,ct,f,d,a,m):
    if m == "copier":
        # Copier le fichier depuis le dossier depart vers le dossier arrivee
        shutil.copy(d+"\\"+f, a+"\\"+f)
    elif m == "deplacer":
        shutil.move(d+"\\"+f, a+"\\"+f)
    return True    
