###
#   OpenDATA : fonctions métiers
###

# Importer les bibliotheques Python
import os
import datetime
import sqlite3
import shutil
import glob

# Importer les fonctions métiers
from fonctions.fonctions_communes import *
from fonctions.fonctions_bdd import *
from fonctions.fonctions_opendata import *

# Definir les variables utiles
conf = {}
ext = []

# Definir les variables de configuration
conf['sqlite'] = "C:\\Users\\damien\\OneDrive\\Programmation\\python\\opendata\\opendata.sqlite"

# Se connecter a la base SQLITE
connecter = sqlite3.connect(conf['sqlite'])
connexion = connecter.cursor()

# Charger les extensions
ext = _bdd_charger_extensions(connexion,connecter,1)