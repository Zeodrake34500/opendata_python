###
#   OpenDATA : fonctions pour la gestion de la BDD
###

import sqlite3

# Configuration : charger la configuration
def _bdd_conf_charger_configuration(cx,ct):
    # Definir les variables utiles
    d = {}
    # Requete
    rq = "SELECT * FROM configuration WHERE actif_on=1;"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    # Recuperer les resultats
    ls = cx.fetchall()
    for l in ls:
        d.update({l[2] : { 'id' : l[0], 'label' : l[1], 'code' : [2], 'valeur' : l[3], 'actif' : l[4] }})
    return d

# Dossiers : charger la liste des dossiers à scanner
def _bdd_charger_dossiers(cx,ct):
    # Definir les variables utiles
    d = {}
    # Requete
    rq = "SELECT * FROM dossiers WHERE actif_on=1;"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    # Recuperer les resultats
    ls = cx.fetchall()
    for l in ls:
        d.update({l[0] : {'id' : l[0], 'label' : l[1], 'dossier' : l[2], 'dossier_scan_id' : [3], 'actif_on' : l[4]}})
        # Requete pour rechercher le dossier de depart par rapport a un dossier d'arrivee
        rq = "SELECT * FROM dossiers WHERE id=" + l[4] + " actif_on=1;"
        cx.execute(rq)
        ct.commit()
        zs = cx.fetchall()
        for z in zs:
            d.update({l[0] : {'dossier_scan' : z[2]}})
    return d

# BDD : jouter une relation
def _bdd_ajouter_relation(cx,ct,s_id,f_id):
    # Requete
    rq = "INSERT INTO rel_scan_fichier('scan_id','fichier_id') VALUES ('" + s_id + "','" + f_id + "');"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    return cx.lastrowid

# BDD : ajouter un scan
def _bdd_ajouter_scan(cx,ct,tps):
   # Requete
    rq = "INSERT INTO scans ('datetime_depart') VALUES ('" + tps + "');"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    return cx.lastrowid    

# BDD : mettre a jour un scan
def _bdd_maj_scan(cx,ct,d):
    # Requete
    rq = "UPDATE scans SET "
    rq += "date_debut_scan='" + str(d['date_debut']) + "', "
    rq += "heure_debut_scan='" + str(d['heure_debut']) + "', "
    rq += "date_fin_scan='" + str(d['date_fin']) + "', "
    rq += "heure_fin_scan='" + str(d['heure_fin']) + "', "
    rq += "nombre_fichiers='" + str(d['nombre_fichiers']) + "'"
    #rq += "datetime_depart='" + d['datetime_depart'] + "', "
    #rq += "datetime_fin='" + d['datetime_fin'] + " "
    rq += " WHERE id=" + str(d['id']) + ";"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    return True

# BDD : verifier fichier scanne
def _bdd_verifier_fichier_scanne(cx,ct,f):
    # Requete
    rq = "SELECT * FROM fichiers WHERE "
    rq += "titre='" + f + "' AND transfert_on=1 AND scan_on=1;"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    ls = cx.fetchall()
    print(ls)
    for l in ls:
        return True
    return False

# BDD : verifier si fichier deja present en base
def _bdd_verifier_fichier_deja_present(cx,ct,fi):
    a_on = bool()
    a_on = False
    # Verifier si le fichier a deja ete enregistre
    rq = "SELECT * FROM fichiers WHERE "
    rq += "titre='" + fi['fichier'] + "' "
    rq += "AND date_creation='" + fi['date_creation'] + "' "
    rq += "AND heure_creation='" + fi['heure_creation'] + "';"
    cx.execute(rq)
    ct.commit()
    lg = cx.fetchall()   
    for l in lg:
        # s'il y a des enregistrements ...
        a_on = True
    return a_on

# BDD : ajouter un fichier a la base
def _bdd_ajouter_fichier(cx,ct,d):
    # Requete
    rq = "INSERT INTO fichiers ('titre','chemin','date_creation','heure_creation', scan_on) "
    rq += "VALUES ('" + d['fichier'] +"','" + d['chemin_fichier'] + "','" + d['date_creation'] + "','" + d['heure_creation'] + "',1);"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    return cx.lastrowid

# BDD : Mettre a jour le fichier transfere
def _bdd_maj_fichier_transfere(cx,ct,tpl):
    # Requete
    rq = "UPDATE fichiers SET "
    rq += "transfert_on=1, "
    rq += "scan_on=1, "
    rq += "date_transfert='" + tpl[1] + "', "
    rq += "datetime_transfert='" + tpl[2] + " "
    rq += "WHERE fichier='" + tpl[0] + "'"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    return True

# BDD : Charger les extensions
def _bdd_charger_extensions(cx,ct,a):
    # Definir les variables utiles
    l = list()
    # Requete
    rq = "SELECT * FROM extensions WHERE actif_on=1 AND authorisation_on=" + a + ";"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    lgs = cx.fetchall()
    for lg in lgs:
        l.append(lg[2])
    return l