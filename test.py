import os
import datetime


dossier = "C:\\Users\\damien\\desktop\\tmp\\python\\depart"


    
    
def _scanner_dossier(dossier):
    # Définir les variables utiles
    df = {}
    # Récupérer le contenu du dossier (liste des fichiers)
    sc = os.listdir(dossier)
    # Parcourir la liste des fichiers trouves
    for f in sc:
        # Creer le chemin absolu vers le fichier cible
        cf = dossier + "\\" + f
        # Rechercher les donnees de temps
        z = os.path.getmtime(cf)
        dt = datetime.datetime.fromtimestamp(z)
        t = dt.strftime("%d-%m-%Y %H:%M:%S")
        # Separer la date et l'heure au niveau du STRFTIME
        d = t.split(' ')
        # Mettre a jour le dictionnaire des donnees des fichiers
        df.update({
            f : {
                'fichier' : f,
                'chemin_absolu' : dossier + "\\" + f,
                'datetime' : dt,
                'date' : d[0],
                'heure' : d[1]
            }
        })
    return df


print _scanner_dossier(dossier)