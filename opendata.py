###
#   OpenDATA
#   Script pour déposer des jeux de données sur une plate-forme
###

# Importer les bibliotheques
import os
import datetime
import sqlite3
import shutil

# Importer les fonctions
execfile('fonctions_communes.py')
execfile('fonctions_bdd.py')
execfile('fonctions_opendata.py')

# Declarer les variables utiles
## Les dictionnaires