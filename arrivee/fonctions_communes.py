###
#   OpenDATA : fonctions utiles
###

import os
import glob
import shutil
import datetime
import sqlite3

# Configuration : charger la configuration
def _conf_charger_configuration(cx,ct):
    # Definir les variables utiles
    d = {}
    # Requete
    rq = "SELECT * FROM configuration WHERE actif_on=1;"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    # Recuperer les resultats
    ls = cx.fetchall()
    for l in ls:
        d.update({l[2] : { 'id' : l[0], 'label' : l[1], 'code' : [2], 'valeur' : l[3], 'actif' : l[4] }})
    return d

# Dossiers : charger la liste des dossiers à scanner
def _charger_dossiers(cx,ct):
    # Definir les variables utiles
    d = {}
    # Requete
    rq = "SELECT * FROM dossiers WHERE actif_on=1;"
    # Executer la requete
    cx.execute(rq)
    ct.commit()
    # Recuperer les resultats
    ls = cx.fetchall()
    for l in ls:
        d.update({l[0] : {'id' : l[0], 'label' : l[1], 'dossier' : l[2], 'dossier_scan_id' : [3], 'actif_on' : l[4]}})
        # Requete pour rechercher le dossier de depart par rapport a un dossier d'arrivee
        rq = "SELECT * FROM dossiers WHERE id=" + l[4] + " actif_on=1;"
        cx.execute(rq)
        ct.commit()
        zs = cx.fetchall()
        for z in zs:
            d.update({l[0] : {'dossier_scan' : z[2]}})
    return d

# Transformer DICT en LIST
def _transformer_dict_list(d):
    l = [ [k,v] for k, v in d.items() ]
    return l

# Scanner le dossier de depart
def _scanner_dossier(d):
    # Definir les variables utiles
    df = {}
    tpl = ()
    lst = []
    # Recuperer le contenu du dossier (liste des fichiers)
    sc = os.listdir(d)
    # Parcourir la liste des fichiers trouves
    for f in sc:
        # Creer le chemin absolu vers le fichier cible
        cf = d + '\\' + f
        # Rechercher les donnees de temps
        z = os.path.getmtime(cf)
        dt = datetime.datetime.fromtimestamp(z)
        t = dt.strftime("%d-%m-%Y %H:%M:%S")
        # Separer la date et l'heure au niveau du STRFTIME
        y = t.split(' ')
        # Mettre a jour le dictionnaire des donnees des fichiers
        tpl = (f,d+"\\"+f,dt,y[0],y[1])
        lst.append(tpl)
        df.update({
            f : {
                'fichier' : f,
                'chemin_fichier' : d + "\\" + f,
                'datetime' : dt,
                'date_creation' : y[0],
                'heure_creation' : y[1]
            }
        })
    return df, lst    

# Chercher le fichier le plus recent
def _chercher_fichier_recent(ch):
    # Rechercher le fichier le plus recent dans le dossier cible
    fr = max(os.listdir(ch), key=lambda f: os.path.getctime("{}/{}".format(ch, f)))
    # Creer le chemin absolu du fichier le plus recent
    cf= ch + "\\" + fr
    # Recuperer sa date et son heure de creation
    z = os.path.getmtime(cf)
    t = datetime.datetime.fromtimestamp(z)
    t = t.strftime("%d-%m-%Y %H:%M:%S")
    d = t.split(' ')
    # Renvoyer un dictionnaire Python
    df = {
        'fichier' : fr,
        'chemin' : ch,
        'chemin_fichier' : cf, 
        'date_complete' : t,
        'date_creation' : d[0],
        'heure_creation' : d[1]
    }
    return df

# Copier le fichier du dossier source vers le dossier de destination
def _copier_fichier(cx,ct,f,d,a,m):
    if m == "copier":
        # Copier le fichier depuis le dossier depart vers le dossier arrivee
        shutil.copy(d+"\\"+f, a+"\\"+f)
    elif m == "deplacer":
        shutil.move(d+"\\"+f, a+"\\"+f)
    return True    
